
# Script loader


Provides a convenient wrapper to add scripts that can have custom parameters

### Installation

```
$ npm install --save @dooksa/script-loader
```

### Usage

```js

import ScriptLoader from '@dooksa/script-loader'

const script = new ScriptLoader({
  id: 'googlem-maps-script',
  src: 'https://maps.googleapis.com/maps/api/js?key=&libraries=',
  customParams: ['key', 'libraries']  // optional,
  apiVars: 'google'  // optional
})

script.load({
  key: 'API_KEY',
  libraries: 'places'
})
  .then((result) => {
    const { google } = result
    const autocomplete = new google.maps.places.AutocompleteService()
  })
  .catch(error => {
    console.error(error)
  })

```