function ScriptLoader ({
  id,
  async,
  crossorigin,
  defer,
  integrity,
  nonce,
  referrerpolicy,
  src,
  type = 'text/javascript',
  nomodule,
  customParams = [],
  apiVars
}) {
  this.queue = []
  this.loaded = false
  this.id = `ds__${id}`
  this.async = async
  this.crossorigin = crossorigin
  this.defer = defer
  this.integrity = integrity
  this.nonce = nonce
  this.referrerpolicy = referrerpolicy
  this.src = src
  this.type = type
  this.nomodule = nomodule
  this.customParams = customParams
  this.apiVars = apiVars
}

ScriptLoader.prototype.loading = function () {
  return Promise.all(this.queue)
}

ScriptLoader.prototype.load = function (config) {
  const loadScript = new Promise((resolve, reject) => {
    const url = this.createUrl(config)

    this.setScript({ url, resolve, reject })
  })

  this.queue.push(loadScript)

  return loadScript
}

ScriptLoader.prototype.createUrl = function (config) {
  let url = this.src

  for (let i = 0; i < this.customParams.length; i++) {
    const paramKey = this.customParams[i]

    url = url.replace(`${paramKey}=`, `${paramKey}=${config[paramKey]}`)
  }

  return url
}

ScriptLoader.prototype.setScript = function ({ url, resolve, reject }) {
  if (document.getElementById(this.id)) {
    return
  }

  const script = document.createElement('script')

  script.id = this.id
  script.type = this.type
  script.src = url
  script.onerror = () => reject()
  script.onload = () => {
    this.loaded = true
    const result = {}

    if (this.apiVars) {
      for (let i = 0; i < this.apiVars.length; i++) {
        const apiVar = this.apiVars[i]
        result[apiVar] = window[apiVar]
      }
    }

    resolve(result)
  }

  if (this.async) {
    script.async = this.async
  }

  if (this.crossorigin) {
    script.crossorigin = this.crossorigin
  }

  if (this.defer) {
    script.defer = this.defer
  }

  if (this.integrity) {
    script.integrity = this.integrity
  }

  if (this.referrerpolicy) {
    script.referrerpolicy = this.referrerpolicy
  }

  if (this.nomodule) {
    script.nomodule = this.nomodule
  }

  if (this.nonce) {
    script.nonce = this.nonce
  }

  document.head.appendChild(script)
}

export default ScriptLoader
